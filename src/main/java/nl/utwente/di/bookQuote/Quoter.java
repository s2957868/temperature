package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public double getBookPrice(double celsius) {
        return celsius * 9 / 5 + 32;
    }
}
